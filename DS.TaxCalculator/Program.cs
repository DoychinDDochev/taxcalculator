﻿using DS.TaxCalculator.Services;
using System;

namespace DS.TaxCalculator
{
    class Program
    {
        const string exitLetter = "q";

        static void Main(string[] args)
        {
            string value = string.Empty;
            Calculator calculator = new Calculator();

            Console.WriteLine("This a TaxCalculator version 1.0");
            do
            {
                Console.WriteLine($"Enter '{exitLetter}' for exit");
                Console.Write("Enter a gross salary:");
                value = Console.ReadLine();

                decimal result;
                if (decimal.TryParse(value, out result))
                {
                    var calculationResult = calculator.Calculate(result);
                    Console.WriteLine($"Gross salary: {calculationResult.GrossSalary} IDR");
                    Console.WriteLine($"Income tax: {calculationResult.IncomeTax} IDR");
                    Console.WriteLine($"Social Contribution tax: {calculationResult.SocialContribution} IDR");
                    Console.WriteLine($"Net salary: {calculationResult.NetSalary} IDR");
                    Console.WriteLine("");
                }
                else if (value != exitLetter)
                {
                    Console.WriteLine("Can't parse the value!");
                }

            } while (value != exitLetter);

        }
    }
}

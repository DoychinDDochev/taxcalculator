﻿namespace DS.TaxCalculator.Services
{
    public class CalculationResult
    {
        public decimal GrossSalary { get; set; }
        public decimal NetSalary { get; set; }
        public decimal IncomeTax { get; set; }
        public decimal SocialContribution { get; set; }
    }
}

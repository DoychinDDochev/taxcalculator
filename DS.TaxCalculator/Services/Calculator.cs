﻿namespace DS.TaxCalculator.Services
{
    /// <summary>
    /// Service that calculates a net salary from a gross salary
    /// </summary>
    public class Calculator
    {
        const decimal incomeTaxPercent = 0.1m; // 10%
        const decimal socialContributionPercent = 0.15m; // 15%
        const decimal taxThreshold = 1000m;
        const decimal maxSocialContribution = 3000m;

        public Calculator()
        {
        }

        /// <summary>
        /// Calculates a net salary from a gross salary
        /// </summary>
        /// <param name="grossSalary">Gross salary</param>
        /// <returns>Net salary</returns>
        public CalculationResult Calculate(decimal grossSalary)
        {
            decimal netSalary = 0m;
            decimal incomeTax = 0m;
            decimal socialContributionTax = 0m;
            decimal socialContributionThreshold = maxSocialContribution - taxThreshold;

            decimal incomeTaxableSalary = grossSalary - taxThreshold;

            if (incomeTaxableSalary > 0)
            {
                incomeTax = incomeTaxableSalary * incomeTaxPercent;
                socialContributionTax = (incomeTaxableSalary > socialContributionThreshold ? socialContributionThreshold : incomeTaxableSalary) * socialContributionPercent;
            }

            netSalary = grossSalary - incomeTax - socialContributionTax;
            CalculationResult calculationResult = new CalculationResult() { GrossSalary = grossSalary, NetSalary = netSalary, IncomeTax = incomeTax, SocialContribution = socialContributionTax };

            return calculationResult;
        }
    }
}
